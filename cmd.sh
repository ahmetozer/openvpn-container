#!/bin/sh
if [ -f "/var/run/netns/container" ]
then
    exec_command="ip netns exec container"
else
    exec_command="exec"
fi

if [ -z "$1" ]
then
    set -- "server.conf"
fi

ifconfig 

$exec_command /usr/sbin/openvpn $@
