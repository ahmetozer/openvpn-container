FROM alpine

WORKDIR /src/

COPY . .

WORKDIR /etc/openvpn

RUN apk add openvpn
RUN chmod +x /src/cmd.sh

ENTRYPOINT [ "/src/cmd.sh"]